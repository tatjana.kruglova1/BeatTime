/**
 *
 */

$('#btnLogin').click(validateLogin);

function validateLogin() {
	var isValid = false;
	if ($('#username').val() != '') {
		isValid = true;
		$('#username').removeClass('error');
	} else {
		isValid = false;
		$('#username').addClass('error');
	}
	if ($('#password').val() != '') {
		isValid = true;
		$('#password').removeClass('error');
	} else {
		isValid = false;
		$('#password').addClass('error');
	}
	if (isValid) {
        sendLoginData();
    } else {
		alert('Fill out all the fields');
	}
}

function sendLoginData() {
    var username = $('#username').val();
    var password = $('#password').val();
    
    $.ajax({
        url: 'LoginServlet',
        data: {
            username: username,
            password: password
        },
		success: function (data) {
			if (data == 'false') {
				alert('Username not found');
				return;
			} else if (data == "Wrong password") {
                alert('Wrong password');
            } else {
            	$(location).attr('href', ('login.html?user=' + username));
            }
        },
        error: function() {
            alert("LOGIN SERVLET ERROR!");
        }
    });
}