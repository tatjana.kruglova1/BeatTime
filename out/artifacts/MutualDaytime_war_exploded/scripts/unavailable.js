$('#unavailableAt').click(function () {
	$('#unavailableModal').modal({
		show: true
	});
});

function addTimeRow(nr) {
	var row = '<br>'
	row += '<span class="pickTime">From </span>';
	row += '<div class="form-inline pickTime">';
	row += '<select id="hourFrom' + nr + '" name="hourFrom' + nr + '" class="form-control">';
	for (var i = 0; i < 24; i++) {
		if (i < 10) {
			row += '<option value="0' + i + '">0' + i + '</option>';
		} else {
			row += '<option value="' + i + '">' + i + '</option>';
		}
	}
	row += '</select>';
	row += '<select id="minutesFrom' + nr + '" name="minutesFrom' + nr + '" class="form-control pickTime">';
	row += '<option value="00">00</option>';
	row += '<option value="15">15</option>';
	row += '<option value="30">30</option>';
	row += '<option value="45">45</option>';
	row += '</select>';
	row += '</div>';
	row += '<span class="pickTime"> To </span>';
	row += '<div class="form-inline pickTime">';
	row += '<select id="hourTo' + nr + '" name="hourTo' + nr + '" class="form-control pickTime">';
	for (var i = 0; i < 24; i++) {
		if (i < 10) {
			row += '<option value="0' + i + '">0' + i + '</option>';
		} else {
			row += '<option value="' + i + '">' + i + '</option>';
		}
	}
	row += '</select>';
	row += '<select id="minutesTo' + nr + '" name="minutesTo' + nr + '" class="form-control pickTime">';
	row += '<option value="00">00</option>';
	row += '<option value="15">15</option>';
	row += '<option value="30">30</option>';
	row += '<option value="45">45</option>';
	row += '</select>';
	row += '</div>';
	row += '</div>';
	$('#unavailableInput').append(row);
}

addTimeRow(1);