/**
 * 
 */
function validateAll() {
	var isValid = false;
	if (validateUser($('#usernameRegister').val())) {
		isValid = true;
		$('#usernameRegister').removeClass('error');
	} else {
		isValid = false;
		$('#usernameRegister').addClass('error');
	}
	if (validatePassword($('#passwordRegister').val())) {
		isValid = true;
		$('#passwordRegister').removeClass('error');
	} else {
		isValid = false;
		$('#passwordRegister').addClass('error');
	}
	if (validateEmail($('#emailRegister').val())) {
		isValid = true;
		$('#emailRegister').removeClass('error');
	} else {
		isValid = false;
		$('#emailRegister').addClass('error');
	}

	console.log('u:', $('#usernameRegister').val());

	console.log('p:', $('#passwordRegister').val());

	console.log('e:', $('#emailRegister').val());
	if (isValid) {
        sendRegistrationData();
    } else {
		alert('Fill out all the fields');
	}
}

function validateEmail(email) {

	var sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
	var sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
	var sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
	var sQuotedPair = '\\x5c[\\x00-\\x7f]';
	var sDomainLiteral = '\\x5b(' + sDtext + '|' + sQuotedPair + ')*\\x5d';
	var sQuotedString = '\\x22(' + sQtext + '|' + sQuotedPair + ')*\\x22';
	var sDomain_ref = sAtom;
	var sSubDomain = '(' + sDomain_ref + '|' + sDomainLiteral + ')';
	var sWord = '(' + sAtom + '|' + sQuotedString + ')';
	var sDomain = sSubDomain + '(\\x2e' + sSubDomain + ')*';
	var sLocalPart = sWord + '(\\x2e' + sWord + ')*';
	var sAddrSpec = sLocalPart + '\\x40' + sDomain; // complete RFC822 email address spec
	var sValidEmail = '^' + sAddrSpec + '$'; // as whole string

	var reValidEmail = new RegExp(sValidEmail);

	if (reValidEmail.test(email)) {
		return true;
	}

	return false;
}

function validateUser(username) {
	if (username == "") return false;
	else return true;
}

function validatePassword(password) {
	if (password == "") return false;
	else return true;
}

function sendRegistrationData() {
    var username = $('#usernameRegister').val();
    var password = $('#passwordRegister').val();
    var email = $('#emailRegister').val();
    
    $.ajax({
        url: 'RegisterServlet',
        data: {
            usernameRegister: username,
            passwordRegister: password,
            emailRegister: email
        },
        success: function(data) {
            if (data == "ERROR") {
                alert("Username already taken");
            }
            $(location).attr('href', ('login.html?user=' + username));
        },
        error: function() {
            console.log('Register failure');
        }
    });
}