BeatTime project. Find mutual daytime for people to chat.

Contains:
Java back-end using Eclipse Jetty server.
Front-end with html/javascript.
Selenium/Nightwatch tests.
JUnit tests (91.9% coverage).
Ant build.