module.exports = {
  before : function(browser) {
    browser.maximizeWindow();
  },
  'Beat Time Home Page' : function (browser) {
    browser
      .url('http://localhost:8080/beattime/')
      .assert.title('BeatTime')
      //.waitForElementVisible('body', 1000)
      //.setValue('input[type=text]', 'nightwatch')
      .setValue('#YourLoc', 'Hyderabad, Telangana, India')
    .click('.addnewfriend')
      .setValue('#FriendsLoc', 'Helsingi')
      .click('.addnewfriend')
      .setValue('#FriendsLoc2', 'Tallinn')
      //.waitForElementVisible('button[name=btnG]', 1000)
      //.click('button[name=btnG]')
      .pause(2000)
      .assert.hidden('#FriendsLoc4')
      .click('.addnewfriend')
      //.assert.visible('#FriendsLoc3')
    .pause(2000)
      .resizeWindow(600, 1000)
      .pause(2000)
      .click('.findmutual')
      .waitForElementVisible('.outputtext', 6000)
      .assert.containsText('.outputtext', '')
      .end();
  }
};
