module.exports = {
  before : function(browser) {
    browser.maximizeWindow();
  },
  'Login' : function (browser) {
    browser
      .url('http://localhost:8080/beattime/')
      .waitForElementVisible( 'body', 1000 )
      .assert.title('BeatTime')
      .click('#navLogin')
      .verify.visible('#username')
      .verify.visible('#password')
      .setValue('#username', 'hesus')
      .setValue('#password', 'qwerty')
      .pause(2000)
      .click('#btnLogin')
      //.click('button[name=btnG]')
      .pause(2000)
      .assert.elementNotPresent('#navLogin')
      .end();
  }
};
