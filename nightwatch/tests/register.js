module.exports = {
  before : function(browser) {
    browser.maximizeWindow();
  },
  'Register' : function (browser) {
    browser
      .url('http://localhost:8080/beattime/')
      .waitForElementVisible( 'body', 1000 )
      .assert.title('BeatTime')
      .click('.register-button')
      .waitForElementVisible('#registration', 3000)
      .verify.visible('#usernameRegister')
      .verify.visible('#emailRegister')
      .verify.visible('#passwordRegister')
      .setValue('#usernameRegister', 'automaattestimine3')
      .setValue('#passwordRegister', 'testingpassword')
      .setValue('#emailRegister', 'test@test123.ee')
      .pause(2000)
      .click('#btnRegister')
      .pause(2000)
      .end();
  }
};
