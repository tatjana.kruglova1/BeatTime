/**
 * 
 */
var numberOfTimeZones = 2;

$('.addnewfriend').click(function () {
	addNewInputField();
	if (numberOfTimeZones === 5) {
		$('.addnewfriend').hide();
	}
	return;
});

$('.register-button').click(function() {
	$('#registration').modal({
		show: true
	});
});


$('#hours-picker').click(function() {
var options = '<option disabled selected>Hours</option>';
        for (var i = 1; i < 24; i++)
            options += '<option value="' + (i) + '">' + (i) + '</option>';
        $('#hours-picker').html(options).show();
});




function addNewInputField() {
	numberOfTimeZones += 1;
	if (numberOfTimeZones === 3) {
		$('.friendsLoc2').show();
	} else if (numberOfTimeZones === 4) {
		$('.friendsLoc3').show();
	} else if (numberOfTimeZones === 5) {
		$('.friendsLoc4').show();
	}
	return;
}

$('.findmutual').click(function (event) {
	var yourLoc = $('input#YourLoc').val();
	var friendsLoc = $('input#FriendsLoc').val();
	var friendsLocTwo = $('input#FriendsLoc2').val();
	var friendsLocThree = $('input#FriendsLoc3').val();
	var friendsLocFour = $('input#FriendsLoc4').val();

	$('.outputtext').hide();
	hideSpan('#yourTime')
	hideSpan('#friendsTime')
	hideSpan('#friendsTime2')
	hideSpan('#friendsTime3')
	hideSpan('#friendsTime4')
	
	$.ajax({
		url: 'BeattimeServlet',
		data: {
			locationOne: yourLoc,
			locationTwo: friendsLoc,
			locationThree: friendsLocTwo,
			locationFour: friendsLocThree,
			locationFive: friendsLocFour
		},
		success: function (data) {
			if (data === "ERROR") {
				$('#myModal').modal({
					show: true
				});
				return;
			}
			var arr = data.split(/[\[\,\]\ ]/);
			var times = [];
			for (var i = 0; i < arr.length; i++) {
				if (arr[i] !== "") {
					times.push(arr[i]);
				}
			}
			console.log(times);
			buildResponseField(times);
		},
		error: function () {
			$('#myModal').modal({
				show: true
			});
		}
	});
});

function buildResponseField(times) {
	$('.outputtext').show();
	var count = 0;
	for (var i = 0; i < times.length; i++) {
		if (count === 0) {
			addValueToSpan('#yourTime', times[i]);
		}
		if (count === 1) {
			addValueToSpan('#friendsTime', times[i]);
		}
		if (count === 2) {
			addValueToSpan('#friendsTime2', times[i]);
		}
		if (count === 3) {
			addValueToSpan('#friendsTime3', times[i]);
		}
		if (count === 4) {
			addValueToSpan('#friendsTime4', times[i]);
		}
		count++;
	}
}

function addValueToSpan(id, value) {
	$(id).show();
	$(id).find('span').html(value);
}

function hideSpan(id) {
	$(id).hide();
	$(id).find('span').empty();
}

function initAutocomplete() {
    var input = document.getElementById("YourLoc");
    var searchBox = new google.maps.places.SearchBox(input);
    
    var input2 = document.getElementById("FriendsLoc");
    var searchBox = new google.maps.places.SearchBox(input2);
    
    var input3 = document.getElementById("FriendsLoc2");
    var searchBox = new google.maps.places.SearchBox(input3);
    
    var input4 = document.getElementById("FriendsLoc3");
    var searchBox = new google.maps.places.SearchBox(input4);
    
    var input5 = document.getElementById("FriendsLoc4");
    var searchBox = new google.maps.places.SearchBox(input5);
}

$('#menuLogin').on('click.dropdown', function(e) {
    e.show;
    $('#username').focus();
});