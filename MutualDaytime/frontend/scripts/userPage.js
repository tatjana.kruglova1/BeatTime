/**
 * 
 */
var username = '';

function parseURLParams(url) {
	var queryStart = url.indexOf("?") + 1, queryEnd = url.indexOf("#") + 1
			|| url.length + 1, query = url.slice(queryStart, queryEnd - 1), pairs = query
			.replace(/\+/g, " ").split("&"), parms = {}, i, n, v, nv;

	if (query === url || query === "") {
		return;
	}

	for (i = 0; i < pairs.length; i++) {
		nv = pairs[i].split("=");
		n = decodeURIComponent(nv[0]);
		v = decodeURIComponent(nv[1]);
		if (!parms.hasOwnProperty(n)) {
			parms[n] = [];
		}

		parms[n].push(nv.length === 2 ? v : null);
	}
	return parms;
}

function getUsername() {
	var jsonResponse = parseURLParams(document.URL);
	username = jsonResponse.user[0];
	console.log(username);
	$('#user').html(username);
	$('#yourLocation').hide();
	$('input[name="YourLoc"]').attr('value', username);
}

getUsername();

$('#changeTimeZone').click(function() {
	$('#timeZoneModal').modal({
		show : true
	});
});

function changeTimeZone() {
	var newTimeZone = $('#DropDownTimezone').val();
	$.ajax({
		url : 'TimeZoneServlet',
		data : {
			timeZone : newTimeZone,
			user: username
		},
		success : function() {
			console.log('Success');
		},
		error: function() {
			console.log('ERROR');
		}
	});
};

$('#unavailableAt').click(function() {
	$('#unavailableModal').modal({
		show : true
	});
});

var rowNr = 0;

function addTimeRow(nr) {
	var row = '<br>'
	row += '<span class="pickTime">From </span>';
	row += '<div class="form-inline pickTime">';
	row += '<select id="hourFrom' + nr + '" name="hourFrom' + nr
			+ '" class="form-control" onChange="selectChange(' + nr + ')">';
	for (var i = 8; i < 23; i++) {
		if (i < 10) {
			row += '<option value="0' + i + '">0' + i + '</option>';
		} else {
			row += '<option value="' + i + '">' + i + '</option>';
		}
	}
	row += '</select>';
	row += '</div>';
	row += '<span class="pickTime"> To </span>';
	row += '<div class="form-inline pickTime">';
	row += '<select id="hourTo' + nr + '" name="hourTo' + nr
			+ '" class="form-control pickTime">';
	for (var i = 8; i < 23; i++) {
		if (i < 10) {
			row += '<option value="0' + i + '">0' + i + '</option>';
		} else {
			row += '<option value="' + i + '">' + i + '</option>';
		}
	}
	row += '</select>';
	row += '</div>';
	row += '</div>';
	$('#unavailableInput').append(row);
	rowNr++;
}

addTimeRow(rowNr);

function selectChange(nr) {
	var hour = parseInt($('#hourFrom' + nr).val());
	var row = '';
	for (var i = (hour + 1); i < 23; i++) {
		if (i < 10) {
			row += '<option value="0' + i + '">0' + i + '</option>';
		} else {
			row += '<option value="' + i + '">' + i + '</option>';
		}
	}
	$('#hourTo' + nr).html(row);
}

$('#addRow').click(function() {
	if (rowNr < 3) {
		addTimeRow(rowNr);
		if (rowNr === 3) {
			$('#addRow').hide();
		}
	}
});

function resetUnavailable() {
	$.ajax({
		url : 'UnavailableServlet',
		data : {
			input : username
		},
		success : function() {
			console.log('Success');
		},
		error: function() {
			console.log('ERROR');
		}
	});
}

function changeUnavailable() {
	var input = username + '\t';
	for (var i = 0; i < rowNr; i++) {
		input += $('#hourFrom' + i).val() + ':00-';
		input += $('#hourTo' + i).val() + ':00\t';
	}
	input += '\n';
	$.ajax({
		url : 'UnavailableServlet',
		data : {
			input : input
		},
		success : function() {
			console.log('Success');
		},
		error: function() {
			console.log('ERROR');
		}
	});
}