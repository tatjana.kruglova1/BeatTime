package tests;

import static org.junit.Assert.*;

import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

import database.FileInformation;

public class TestFileWriter {
	
	FileInformation writer;

	@Before
	public void setUp() throws Exception {
		writer = new FileInformation();
	}

	@Test
	public void testChekcingUsernameAvailabilityERROR() {
		String availability = writer.checkUsernameAvailability("hesus");
		assertEquals("ERROR", availability);
	}
	
	@Test
	public void testChekcingUsernameAvailabilitySUCESS() {
		String availability = writer.checkUsernameAvailability("asdasdasdsada");
		assertEquals("SUCCESS", availability);
	}
	
	@Test
	public void testUsernameAndPassword() {
		String isfound = writer.readDataFromFile("hesus\tqwerty");
		assertEquals("hesus\tqwerty", isfound);
	}
	
	@Test
	public void testUsernameAndPasswordWrong() {
		String isfound = writer.readDataFromFile("hesus\tqwerasdaty");
		assertEquals("Wrong password", isfound);
	}
	
	@Test
	public void testUsernameAndPasswordBothWrong() {
		String isfound = writer.readDataFromFile("hesdsdsus\tqwerasdaty");
		assertEquals("false", isfound);
	}
	
	@Test
	public void testCreateFile() throws IOException {
		/*FileOutputStream stream = mock(FileOutputStream.class);
		when(new FileOutputStream(anyString(), anyBoolean())).thenReturn(stream);
		doNothing().when(stream).write(anyByte());*/
		
		/*writer.createUser("yoyo", "yoyo", "a@a.ee");
		String content = "yoyo\tyoyo\ta@a.ee\t+02:00\n";
		byte[] line = content.getBytes();
        verify(stream).write(line);
        verify(stream).close();*/
	}

}
