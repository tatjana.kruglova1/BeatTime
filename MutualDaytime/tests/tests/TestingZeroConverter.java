package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import daytime.Converter;

public class TestingZeroConverter {
	
	Converter conv;
	ArrayList<String> comparableUTCs;

	@Before
	public void setUp() throws Exception {
		
		comparableUTCs = new ArrayList<String>();
		
		comparableUTCs.add("+03:45");
		comparableUTCs.add("+12:00");
		comparableUTCs.add("-04:00");
		comparableUTCs.add("+00:00");
		comparableUTCs.add("-10:00");
		
		conv = new Converter(comparableUTCs);
	}

	@Test
	public void test3ToZero() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(4.25, 5.25, 6.25, 7.25, 8.25, 9.25, 10.25, 11.25, 12.25, 13.25, 14.25, 15.25, 16.25, 17.25, 18.25));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(0)), conv.getUTCNumber(comparableUTCs.get(0))));
	}
	
	@Test
	public void test12ToZero() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(20.0, 21.0, 22.0, 23.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(1)), conv.getUTCNumber(comparableUTCs.get(1))));
	}
	
	@Test
	public void testMinus4ToZero() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 0.0, 1.0, 2.0));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(2)), conv.getUTCNumber(comparableUTCs.get(2))));
	}
	
	@Test
	public void test0ToZero() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(3)), conv.getUTCNumber(comparableUTCs.get(3))));
	}
	
	@Test
	public void testMinus10ToZero() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(4)), conv.getUTCNumber(comparableUTCs.get(4))));
	}

}
