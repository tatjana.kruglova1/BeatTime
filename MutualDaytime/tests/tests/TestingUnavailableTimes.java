package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import database.FileInformation;
import daytime.Divider;
import daytime.TimesMatcher;

public class TestingUnavailableTimes {

	TimesMatcher matcher;
	FileInformation writer;
	Divider divider;
	
	@Before
	public void setUp() throws Exception {
		matcher = new TimesMatcher();
		writer = new FileInformation();
		divider = new Divider();
	}
	
	@Test
	public void testTimeWithUsersBasicCase() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("asd");
		locations.add("Tallinn");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("8:00-11:00\t14:00-21:00\t", times.get(0));
		assertEquals("9:00-12:00\t15:00-22:00\t", times.get(1));
	}
	
	@Test
	public void testTimeWithTwoUsers() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("asd");
		locations.add("peeterpaulus");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("8:00-12:00\t14:00-16:00\t", times.get(0));
		assertEquals("14:00-18:00\t20:00-22:00\t", times.get(1));
	}
	
	@Test
	public void testTimeWithTwoUsers2() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("asd");
		locations.add("tanja");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("15:00-20:00\t", times.get(0));
		assertEquals("8:00-13:00\t", times.get(1));
	}
	
	@Test
	public void testTimeWithTwoUsers3() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("asd");
		locations.add("tanja3");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("14:00-17:30\t", times.get(0));
		assertEquals("8:30-12:00\t", times.get(1));
	}
	
	@Test
	public void testTimeWithUsersAndTwoTimes() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("tanja4");
		locations.add("Tallinn");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("8:00-10:00\t13:00-15:00\t19:00-21:00\t", times.get(0));
		assertEquals("9:00-11:00\t14:00-16:00\t20:00-22:00\t", times.get(1));
	}

}
