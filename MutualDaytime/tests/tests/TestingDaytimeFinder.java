package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import daytime.DaytimeFinder;

public class TestingDaytimeFinder {
	
	ArrayList<ArrayList<Double>> group;
	DaytimeFinder finder;

	@Before
	public void setUp() throws Exception {
		group = new ArrayList<ArrayList<Double>>();
		finder = new DaytimeFinder();
	}

	@Test
	public void testMutualTime() {
		ArrayList<Double> hours = new ArrayList<Double>(Arrays.asList(8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0));
		group.add(hours);
		ArrayList<Double> hours1 = new ArrayList<Double>(Arrays.asList(14.5, 15.0, 16.0, 17.0, 18.5, 19.0));
		group.add(hours1);
		
		ArrayList<Double> mutualZeroTime = new ArrayList<Double>(Arrays.asList(14.0, 15.0, 16.0, 17.0, 18.5, 19.0));
		
		assertEquals(mutualZeroTime, finder.compareHours(group));
	}
	
	@Test
	public void testMutualTimeTwo() {
		ArrayList<Double> hours = new ArrayList<Double>(Arrays.asList(8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.5, 15.5));
		group.add(hours);
		ArrayList<Double> hours1 = new ArrayList<Double>(Arrays.asList(14.25, 15.5, 16.75, 17.0, 18.0, 19.5));
		group.add(hours1);
		
		ArrayList<Double> mutualZeroTime = new ArrayList<Double>(Arrays.asList(14.25, 15.5));
		
		assertEquals(mutualZeroTime, finder.compareHours(group));
	}
	
	@Test
	public void testMutualTimeThree() {
		ArrayList<Double> hours = new ArrayList<Double>(Arrays.asList(8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0));
		group.add(hours);
		ArrayList<Double> hours1 = new ArrayList<Double>(Arrays.asList(14.0, 15.0, 16.0, 17.0, 18.0, 19.0));
		group.add(hours1);
		ArrayList<Double> hours2 = new ArrayList<Double>(Arrays.asList(10.0, 11.0, 12.0, 13.0, 14.0, 15.5));
		group.add(hours2);
		
		ArrayList<Double> mutualZeroTime = new ArrayList<Double>(Arrays.asList(14.0, 15.5));
		
		assertEquals(mutualZeroTime, finder.compareHours(group));
	}
	
	@Test
	public void testMutualTimeFour() {
		ArrayList<Double> hours = new ArrayList<Double>(Arrays.asList(8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.75, 16.0, 17.0));
		group.add(hours);
		ArrayList<Double> hours1 = new ArrayList<Double>(Arrays.asList(14.0, 15.25, 16.0, 17.0, 18.0, 19.0));
		group.add(hours1);
		ArrayList<Double> hours2 = new ArrayList<Double>(Arrays.asList(10.0, 11.0, 12.0, 13.0, 14.0, 15.5, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0));
		group.add(hours2);
		ArrayList<Double> hours3 = new ArrayList<Double>(Arrays.asList(13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0));
		group.add(hours3);
		
		ArrayList<Double> mutualZeroTime = new ArrayList<Double>(Arrays.asList(14.0, 15.75, 16.0, 17.0));
		
		assertEquals(mutualZeroTime, finder.compareHours(group));
	}
	
	@Test
	public void testMutualTimeFive() {
		ArrayList<Double> hours = new ArrayList<Double>(Arrays.asList(8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0));
		group.add(hours);
		ArrayList<Double> hours1 = new ArrayList<Double>(Arrays.asList(14.0, 15.0, 16.0, 17.0, 18.0, 19.0));
		group.add(hours1);
		ArrayList<Double> hours2 = new ArrayList<Double>(Arrays.asList(10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0));
		group.add(hours2);
		ArrayList<Double> hours3 = new ArrayList<Double>(Arrays.asList(13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0));
		group.add(hours3);
		ArrayList<Double> hours4 = new ArrayList<Double>(Arrays.asList(17.0, 18.0, 19.0, 20.0, 21.0, 22.0));
		group.add(hours4);
		
		ArrayList<Double> mutualZeroTime = new ArrayList<Double>();
		mutualZeroTime.addAll(Arrays.asList(17.0));
		
		assertEquals(mutualZeroTime, finder.compareHours(group));
	}
	
	@Test
	public void testMutualTimeNotFound() {
		ArrayList<Double> hours = new ArrayList<Double>(Arrays.asList(8.0, 9.0, 10.0, 11.0, 12.0));
		group.add(hours);
		ArrayList<Double> hours1 = new ArrayList<Double>(Arrays.asList(14.0, 15.0, 16.0, 17.0, 18.0, 19.0));
		group.add(hours1);
		
		ArrayList<Double> mutualZeroTime = new ArrayList<Double>();
		
		assertEquals(mutualZeroTime, finder.compareHours(group));
	}


}
