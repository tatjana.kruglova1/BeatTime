package tests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;

import beattimeservlet.RegisterServlet;
import database.FileInformation;

public class RegisterServletTests {
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	private RegisterServlet servlet;
	private StringWriter stringWriter;
	private PrintWriter printWriter;
	private FileInformation writer;
	
	@Before
	public void setUp() throws FileNotFoundException {
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		servlet = new RegisterServlet();
		stringWriter = new StringWriter();
		printWriter  = new PrintWriter(stringWriter);
		writer = mock(FileInformation.class);
		//output = spy(new FileOutputStream("MutualDaytime/resources/database.txt", true));
	}

	@Test
	public void registerTest() throws IOException, ServletException {
		when(request.getParameter("usernameRegister")).thenReturn("saklfshfakls");
		when(request.getParameter("passwordRegister")).thenReturn("asd");
		when(request.getParameter("emailRegister")).thenReturn("asd@asd.ee");
		when(response.getWriter()).thenReturn(printWriter);
		//doNothing().doThrow(new RuntimeException()).when(writer).createUser(anyString(), anyString(), anyString());
		//doNothing().doThrow(new RuntimeException()).when(output).write(anyByte());
		/*servlet = mock(RegisterServlet.class);
		doNothing().doThrow(new RuntimeException()).when(servlet).doGet(request, response);
		servlet.doGet(request, response);
		//verify(output).write(anyByte());
		String result = stringWriter.getBuffer().toString().trim();
		assertEquals(result, "");*/
	}
	
	@Test
	public void userTakenTest() throws IOException, ServletException {
		when(request.getParameter("usernameRegister")).thenReturn("hesus");
		when(request.getParameter("passwordRegister")).thenReturn("asd");
		when(request.getParameter("emailRegister")).thenReturn("asd@asd.ee");
		when(response.getWriter()).thenReturn(printWriter);
		doNothing().when(writer).createUser("hesus", "asd", "asd@asd.ee");
		servlet.doGet(request, response);
		String result = stringWriter.getBuffer().toString().trim();
		assertEquals(result, "ERROR");
	}
	
	@Test
	public void emptyTest() throws IOException, ServletException {
		when(request.getParameter("usernameRegister")).thenReturn("");
		when(request.getParameter("passwordRegister")).thenReturn("asd");
		when(request.getParameter("emailRegister")).thenReturn("asd@asd.ee");
		when(response.getWriter()).thenReturn(printWriter);
		servlet.doGet(request, response);
		String result = stringWriter.getBuffer().toString().trim();
		assertEquals(result, "");
	}
	
	@Test
	public void nullTest() throws IOException, ServletException {
		when(request.getParameter("usernameRegister")).thenReturn(null);
		when(request.getParameter("passwordRegister")).thenReturn("asd");
		when(request.getParameter("emailRegister")).thenReturn("asd@asd.ee");
		when(response.getWriter()).thenReturn(printWriter);
		servlet.doGet(request, response);
		String result = stringWriter.getBuffer().toString().trim();
		assertEquals(result, "");
	}

}