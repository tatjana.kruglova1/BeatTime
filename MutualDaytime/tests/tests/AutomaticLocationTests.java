package tests;

import static org.junit.Assert.*;
import java.time.ZoneId;
import java.time.ZoneOffset;
import org.junit.Test;
import location.AutomaticLocation;
import location.LocationSearch;
import static org.mockito.Mockito.*;

public class AutomaticLocationTests {
	AutomaticLocation test = new AutomaticLocation();
	LocationSearch test2 = new LocationSearch();

	@Test
	public void testTallinn() {
		String location = "tallinn";
		String timeZoneID = test2.getTimeZoneID(location);
		String windowsTimezone = test.getWindowsTimezone();
		boolean isValidLocation = test2.isValidLocation(location);
		if (isValidLocation) {
			if (test2.getUTCTimeZone(timeZoneID).equals(windowsTimezone)) {
				if (test.isSummerTime()) {
					assertEquals("+03:00", windowsTimezone);
					assertNotEquals("+02:00", windowsTimezone);
					//assertEquals("+04:00", windowsTimezone); //will fail
				} else {
					assertEquals("+02:00", windowsTimezone);
					assertNotEquals("+03:00", windowsTimezone);
					//assertEquals("+04:00", windowsTimezone); //will fail
				}
			}
		}
	}
	
	@Test
	public void testNewYork() {
		String location = "new york";
		String timeZoneID = test2.getTimeZoneID(location);
		boolean isValidLocation = test2.isValidLocation(location);
		test = mock(AutomaticLocation.class);
		when(test.getWindowsTimezone()).thenReturn(test2.getUTCTimeZone(timeZoneID));
		when(test.isSummerTime()).thenReturn(test2.isSummerTime(timeZoneID));
		String windowsTimezone = test.getWindowsTimezone();
		if (isValidLocation) {
			if (test2.getUTCTimeZone(timeZoneID).equals(windowsTimezone)) {
				if (test.isSummerTime()) {
					assertEquals("-04:00", windowsTimezone);
					assertNotEquals("-05:00", windowsTimezone);
					//assertEquals("+04:00", windowsTimezone); //will fail
				} else {
					assertEquals("-05:00", windowsTimezone);
					assertNotEquals("-04:00", windowsTimezone);
				}
			}
		}
	}
	
	@Test
	public void testReykjavik() {
		String location = "reykjavik";
		String timeZoneID = test2.getTimeZoneID(location);
		test = mock(AutomaticLocation.class);
		when(test.getZoneID()).thenReturn(ZoneId.of("Atlantic/Reykjavik"));
		when(test.getOffset()).thenReturn(ZoneOffset.of("Z"));
		when(test.getWindowsTimezone()).thenReturn(test2.getUTCTimeZone(timeZoneID));
		String windowsTimezone = test.getWindowsTimezone();
		boolean isValidLocation = test2.isValidLocation(location);
		String locationTimeZone = test2.getUTCTimeZone(timeZoneID);
		if (isValidLocation) {
			if (locationTimeZone.equals(windowsTimezone)) {
				assertEquals("+00:00", windowsTimezone);
				assertNotEquals("-05:00", windowsTimezone);
			}
		}
	}
}
