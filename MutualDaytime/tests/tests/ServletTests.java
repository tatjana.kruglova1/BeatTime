package tests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.junit.Before;
import org.junit.Test;

import beattimeservlet.BeattimeServlet;

public class ServletTests {
	private HttpServletRequest request;
	private HttpServletResponse response;
	private BeattimeServlet servlet;
	private StringWriter sw;
	private PrintWriter pw;
	
	@Before
	public void setUp() {
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		servlet = new BeattimeServlet();
		sw = new StringWriter();
		pw  = new PrintWriter(sw);
	}

	@Test
	public void testServletTwoLocations() throws ServletException, IOException {
		when(request.getParameter("locationOne")).thenReturn("Tallinn");
		when(request.getParameter("locationTwo")).thenReturn("London");
		when(response.getWriter()).thenReturn(pw);
		servlet.doGet(request, response);
		String result = sw.getBuffer().toString().trim();
		assertEquals("[10:00-22:00\t, 8:00-20:00\t]", result);		
	}
	
	@Test
	public void testServletThreeLocations() throws ServletException, IOException {
		when(request.getParameter("locationOne")).thenReturn("Tallinn");
		when(request.getParameter("locationTwo")).thenReturn("London");
		when(request.getParameter("locationThree")).thenReturn("Moscow");
		when(response.getWriter()).thenReturn(pw);
		servlet.doGet(request, response);
		String result = sw.getBuffer().toString().trim();
		assertEquals("[10:00-22:00\t, 8:00-20:00\t, 10:00-22:00\t]", result);	
	}

	@Test
	public void testServletFourLocations() throws ServletException, IOException {
		when(request.getParameter("locationOne")).thenReturn("Tallinn");
		when(request.getParameter("locationTwo")).thenReturn("London");
		when(request.getParameter("locationThree")).thenReturn("Moscow");
		when(request.getParameter("locationFour")).thenReturn("Tokyo");
		when(response.getWriter()).thenReturn(pw);
		servlet.doGet(request, response);
		String result = sw.getBuffer().toString().trim();
		assertEquals("[10:00-16:00\t, 8:00-14:00\t, 10:00-16:00\t, 16:00-22:00\t]", result);	
	}
	
	@Test
	public void testServletFiveLocations() throws ServletException, IOException {
		when(request.getParameter("locationOne")).thenReturn("Tallinn");
		when(request.getParameter("locationTwo")).thenReturn("London");
		when(request.getParameter("locationThree")).thenReturn("Moscow");
		when(request.getParameter("locationFour")).thenReturn("Tokyo");
		when(request.getParameter("locationFive")).thenReturn("Sydney");
		when(response.getWriter()).thenReturn(pw);
		servlet.doGet(request, response);
		String result = sw.getBuffer().toString().trim();
		assertEquals("[10:00-15:00\t, 8:00-13:00\t, 10:00-15:00\t, 16:00-21:00\t, 17:00-22:00\t]", result);	
	}
	
	@Test
	public void testEmpty() throws ServletException, IOException {
		when(response.getWriter()).thenReturn(pw);
		servlet.doGet(request, response);
		String result = sw.getBuffer().toString().trim();
		assertEquals("ERROR", result);
	}
}
