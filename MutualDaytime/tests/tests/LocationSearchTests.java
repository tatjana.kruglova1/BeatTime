package tests;

import static org.junit.Assert.*;
import java.net.MalformedURLException;
import java.net.URL;
import org.junit.Test;
import location.LocationSearch;

public class LocationSearchTests {

	LocationSearch test = new LocationSearch();

	@Test
	public void isValidLocationTest() {
		assertTrue(test.isValidLocation("tallinn"));
		assertFalse(test.isValidLocation("hehsrgjrgjsttyef"));
		assertTrue(test.isValidLocation("new york"));
		assertTrue(test.isValidLocation("r�uge, v�rumaa"));
		assertTrue(test.isValidLocation("london, usa"));
	}
	
	@Test
	public void createXMLStringTest() throws MalformedURLException {
		assertTrue(test.createXMLString(new URL("https://maps.googleapis.com"
				+ "/maps/api/geocode/xml?address=tallinn&sensor=false")).contains("<result"));
		assertFalse(test.createXMLString(new URL("https://maps.googleapis.com"
				+ "/maps/api/geocode/xml?address=dsgsdgsdgsd&sensor=false")).contains("<result"));
		assertTrue(test.createXMLString(new URL("https://maps.googleapis.com"
				+ "/maps/api/geocode/xml?address=new%20york&sensor=false")).contains("<result"));
		assertTrue(test.createXMLString(new URL("https://maps.googleapis.com"
				+ "/maps/api/geocode/xml?address=bengaluru&sensor=false")).contains("<result"));
		assertFalse(test.createXMLString(new URL("https://maps.googleapis.com"
				+ "/maps/api/geocode/xml?address=hhiiopphhjfg&sensor=false")).contains("<result"));
		assertTrue(test.createXMLString(new URL("https://maps.googleapis.com/maps"
				+ "/api/timezone/xml?location=64,35&timestamp=1331161200&sensor=false")).contains("<raw"));
		assertTrue(test.createXMLString(new URL("https://maps.googleapis.com/maps"
				+ "/api/timezone/xml?location=59,24&timestamp=1331161200&sensor=false")).contains("<raw"));
		assertFalse(test.createXMLString(new URL("https://maps.googleapis.com/maps"
				+ "/api/timezone/xml?location=43,132&timestamp=1331161200&sensor=false")).contains("<raw"));

		
	}
	
	@Test
	public void getTimeZoneIDTest() {
		assertEquals("America/New_York", test.getTimeZoneID("new york"));
		assertEquals("Europe/Tallinn", test.getTimeZoneID("tallinn"));
		assertNull(test.getTimeZoneID("sghjsjsgjsrsgjsfgj"));
	}
	
	@Test
	public void getUTCTimeZoneNewYork() {
		if (test.isSummerTime(test.getTimeZoneID("new york"))) {
			assertEquals("-04:00", test.getUTCTimeZone("America/New_York"));
			assertNotEquals("-05:00", test.getUTCTimeZone("America/New_York"));
		} else {
			assertEquals("-05:00", test.getUTCTimeZone("America/New_York"));
			assertNotEquals("-04:00", test.getUTCTimeZone("America/New_York"));
		}
	}
	
	@Test
	public void getUTCTimeZoneTallinn() {
		if (test.isSummerTime(test.getTimeZoneID("tallinn"))) {
			assertEquals("+03:00", test.getUTCTimeZone("Europe/Tallinn"));
			assertNotEquals("+02:00", test.getUTCTimeZone("Europe/Tallinn"));
		} else {
			assertNotEquals("+03:00", test.getUTCTimeZone("Europe/Tallinn"));
			assertEquals("+02:00", test.getUTCTimeZone("Europe/Tallinn"));
		}
	}
	
	@Test
	public void getUTCTimeZoneReykjavik() {
		assertEquals("+00:00", test.getUTCTimeZone(test.getTimeZoneID("reykjavik")));
		assertNotEquals("+02:00", test.getUTCTimeZone(test.getTimeZoneID("reykjavik")));
	}
	
	@Test (expected = NumberFormatException.class)
	public void getCoordinateException() {
		assertNotNull(test.getCoordinate("n>maria<", "n"));
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void getCoordinateException2() {
		assertNotNull(test.getCoordinate("n>1<", "r"));
	}
	
	@Test
	public void getCoordinateTest() {
		assertEquals(1.064, test.getCoordinate("n>1.064<", "n"), 0.1);
	}
	
	@Test
	public void getTimeZoneXMLTest() {
		String searchString = "<time_zone_id>tallinn<";
		assertEquals("tallinn", test.getTimeZoneIDFromXML(searchString));
		searchString = "<time_zone_id>new york<";
		assertEquals("new york", test.getTimeZoneIDFromXML(searchString));
	}
}
