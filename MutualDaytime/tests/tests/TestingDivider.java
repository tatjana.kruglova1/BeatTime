package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import daytime.Divider;

public class TestingDivider {

	Divider divider;
	@Before
	public void setUp() throws Exception {
		divider = new Divider();
	}

	@Test
	public void testWithOneTime() {
		String times = "13:00-15:00";	
		List<Double> timestoDouble = new ArrayList<Double>();
		timestoDouble.add(14.0);
		assertEquals(timestoDouble,divider.dividedUnavailableTimes(times));
	}

	@Test
	public void testWithTwoTimes() {
		String times = "13:00-15:00\t16:00-18:00";	
		List<Double> timestoDouble = new ArrayList<Double>();
		timestoDouble.add(14.0);
		timestoDouble.add(17.0);
		assertEquals(timestoDouble, divider.dividedUnavailableTimes(times));
	}
}
