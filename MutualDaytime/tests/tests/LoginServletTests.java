package tests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import beattimeservlet.LoginServlet;

public class LoginServletTests {
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	private LoginServlet servlet;
	private StringWriter stringWriter;
	private PrintWriter printWriter;
	
	@Before
	public void setUp() {
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		servlet = new LoginServlet();
		stringWriter = new StringWriter();
		printWriter  = new PrintWriter(stringWriter);
	}

	@Test
	public void logInTest() throws IOException, ServletException {
		when(request.getParameter("username")).thenReturn("hesus");
		when(request.getParameter("password")).thenReturn("qwerty");
		when(response.getWriter()).thenReturn(printWriter);
		servlet.doGet(request, response);
		String result = stringWriter.getBuffer().toString().trim();
		assertEquals(result, "hesus\tqwerty");
	}
	
	@Test
	public void emptyTest() throws IOException, ServletException {
		when(request.getParameter("username")).thenReturn("");
		when(request.getParameter("password")).thenReturn("");
		when(response.getWriter()).thenReturn(printWriter);
		servlet.doGet(request, response);
		String result = stringWriter.getBuffer().toString().trim();
		assertEquals(result, "");
	}
	
	@Test
	public void nullTest() throws IOException, ServletException {
		when(request.getParameter("username")).thenReturn(null);
		when(request.getParameter("password")).thenReturn("");
		when(response.getWriter()).thenReturn(printWriter);
		servlet.doGet(request, response);
		String result = stringWriter.getBuffer().toString().trim();
		assertEquals(result, "");
	}

}
