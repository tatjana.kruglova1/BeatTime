package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import daytime.Converter;

public class TestingOriginalConverter {

	Converter conv;
	ArrayList<String> comparableUTCs;

	@Before
	public void setUp() throws Exception {
		
		comparableUTCs = new ArrayList<String>();
		
		comparableUTCs.add("+03:45");
		comparableUTCs.add("+12:00");
		comparableUTCs.add("-04:00");
		comparableUTCs.add("+00:00");
		comparableUTCs.add("-10:00");
		
		conv = new Converter(comparableUTCs);

		ArrayList<Double> mutualZeroTime = new ArrayList<>(Arrays.asList(8.25, 9.0, 10.0, 11.0, 12.25, 13.75, 14.0, 15.25, 16.0, 17.0, 18.75, 19.0, 20.25, 21.0, 22.0));
		conv.setHours(mutualZeroTime);
		conv.setToZero(false);
	}

	@Test
	public void test3ToOriginal() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(12.0, 12.75, 13.75, 14.75, 16.0, 17.5, 17.75, 19.0, 19.75, 20.75, 22.5, 22.75, 0.0, 0.75, 1.75));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(0)), conv.getUTCNumber(comparableUTCs.get(0))));
	}
	
	@Test
	public void test12ToOriginal() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(20.25, 21.0, 22.0, 23.0, 0.25, 1.75, 2.0, 3.25, 4.0, 5.0, 6.75, 7.0, 8.25, 9.0, 10.0));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(1)), conv.getUTCNumber(comparableUTCs.get(1))));
	}
	
	@Test
	public void testMinus4ToOriginal() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(4.25, 5.0, 6.0, 7.0, 8.25, 9.75, 10.0, 11.25, 12.0, 13.0, 14.75, 15.0, 16.25, 17.0, 18.0));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(2)), conv.getUTCNumber(comparableUTCs.get(2))));
	}
	
	@Test
	public void test0ToOriginal() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(8.25, 9.0, 10.0, 11.0, 12.25, 13.75, 14.0, 15.25, 16.0, 17.0, 18.75, 19.0, 20.25, 21.0, 22.0));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(3)), conv.getUTCNumber(comparableUTCs.get(3))));
	}
	
	@Test
	public void testMinus10ToOriginal() {
		ArrayList<Double> hours = new ArrayList<>(Arrays.asList(22.25, 23.0, 0.0, 1.0, 2.25, 3.75, 4.0, 5.25, 6.0, 7.0, 8.75, 9.0, 10.25, 11.0, 12.0));
		
		assertEquals("Result", hours, conv.convertHours(conv.getUTCIndicator(comparableUTCs.get(4)), conv.getUTCNumber(comparableUTCs.get(4))));
	}


}
