package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import database.FileInformation;
import daytime.TimesMatcher;
import location.AutomaticLocation;

public class TestingTimeMatcher {

	TimesMatcher matcher;
	AutomaticLocation test;
	FileInformation writer;
	
	@Before
	public void setUp() throws Exception {
		matcher = new TimesMatcher();
		test = new AutomaticLocation();
		writer = new FileInformation();
	}

	@Test
	public void testTallinnBengaluru() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("Tallinn");
		locations.add("Bengaluru");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("8:00-19:30\t", times.get(0));
		assertEquals("10:30-22:00\t", times.get(1));
	}
	
	@Test
	public void testTallinnLondon() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("Tallinn");
		locations.add("London");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("10:00-22:00\t", times.get(0));
		assertEquals("8:00-20:00\t", times.get(1));
	}
	
	@Test
	public void testTallinnLondonNewYork() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("Tallinn");
		locations.add("London");
		locations.add("New York");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		if(test.isSummerTime()) {
			assertEquals("15:00-22:00\t", times.get(0));
			assertEquals("13:00-20:00\t", times.get(1));
			assertEquals("8:00-15:00\t", times.get(2));
		} else {
			assertEquals("15:00-22:00\t", times.get(0));
			assertEquals("13:00-20:00\t", times.get(1));
			assertEquals("8:00-15:00\t", times.get(2));
		}
	}
	
	@Test
	public void testNoTime() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("Tallinn");
		locations.add("London");
		locations.add("New York");
		locations.add("Sydney");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("Could not find mutual time", times.get(0));
	}
	
	@Test
	public void testTimeWithUser() {
		String isInFile = writer.readDataFromFile("asd\tpassword");
		if(isInFile.equals("false")) {
			writer.createUser("yo", "password", "asd@gmail.com");
		}
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("yo");
		locations.add("Moscow");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("8:00-21:00\t", times.get(0));
		assertEquals("9:00-22:00\t", times.get(1));
	}
	
	@Test
	public void testTokyoNewYork() {
		ArrayList<String> locations  = new ArrayList<String>();
		locations.add("Tokyo");
		locations.add("New York");
		ArrayList<String> times = new ArrayList<>();
		times = matcher.getTimezones(locations);
		assertEquals("8:00-11:00\t", times.get(0));
		assertEquals("19:00-22:00\t", times.get(1));
	}

}
