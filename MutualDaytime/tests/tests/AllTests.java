package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AutomaticLocationTests.class, LocationSearchTests.class, TestingDaytimeFinder.class,
		TestingOriginalConverter.class, TestingZeroConverter.class, TestingTimeMatcher.class, 
		ServletTests.class, TestFileWriter.class, RegisterServletTests.class, LoginServletTests.class,
		TestingUnavailableTimes.class, TestingDivider.class})
public class AllTests {

}
