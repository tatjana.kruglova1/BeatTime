package location;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;

public class AutomaticLocation {
	
	public String getWindowsTimezone() {
	    ZoneOffset offset = getOffset();
	    if (offset.toString().equals("Z")) {
	    	return "+00:00";
	    }
		return offset.toString();
	}
	
	public ZoneOffset getOffset() {
		ZoneId zone = getZoneID();
		LocalDateTime dateTime = LocalDateTime.now();
		ZonedDateTime zonedDateTime = dateTime.atZone(zone);
	    ZoneOffset offset = zonedDateTime.getOffset();
	    return offset;
	}
	
	public ZoneId getZoneID() {
		return ZoneId.of(TimeZone.getDefault().getID());
	}
	
	public boolean isSummerTime() {
		Date date = new Date();
		return TimeZone.getDefault().inDaylightTime(date);
		
	}
}
