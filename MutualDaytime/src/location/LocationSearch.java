package location;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;


public class LocationSearch {
	
	public boolean isValidLocation(String location) {
		if (location.contains(" ")) {
			location = location.trim().replace(" ", "%20");
		}
		
		location = refactorLocation(location);
		
		String results = "";
		try {
			URL url = new URL("https://maps.googleapis.com"
					+ "/maps/api/geocode/xml?address=" + location + "&sensor=false");
			results = createXMLString(url);
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (results.contains("<result")) {
			return true;
		}
		return false;
	}
	
	public String createXMLString(URL url) throws MalformedURLException {
		
		StringBuilder sb = null;
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(url.openStream()));
			String line;
			sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		return sb.toString();
	}
	
	public String getTimeZoneID(String location) {
		//System.out.println(location + ": " + isValidLocation(location));
		if (isValidLocation(location)) {
			if (location.contains(" ")) {
				location = location.trim().replace(" ", "%20");
			}
			
			location = refactorLocation(location);
			String geoCoordsXML, timeZoneXML, timeZone = "";
			double lat, lon;
			try {
				geoCoordsXML = createXMLString(new URL("https://maps.googleapis.com"
						+ "/maps/api/geocode/xml?address=" + location + "&sensor=false"));
				Thread.sleep(500);
				
				lat = getCoordinate(geoCoordsXML, "<lat>");
				lon = getCoordinate(geoCoordsXML, "<lng>");
				
				timeZoneXML = createXMLString(new URL("https://maps.googleapis.com/maps"
						+ "/api/timezone/xml?location="
						+ lat + "," + lon + "&timestamp=1331161200&sensor=false"));
				Thread.sleep(500);
				timeZone = getTimeZoneIDFromXML(timeZoneXML);
				
			} catch (MalformedURLException e) {
				System.err.println(e.getMessage());
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
			return timeZone;
		}
		return null; //put something else here
	}
	
	public String getUTCTimeZone(String id) {
		ZoneId zone = ZoneId.of(id);
		LocalDateTime dateTime = LocalDateTime.now();
		ZonedDateTime zonedDateTime = dateTime.atZone(zone);
	    ZoneOffset offset = zonedDateTime.getOffset();
	    if (offset.toString().equals("Z")) {
	    	return "+00:00";
	    }
		return offset.toString();
		
	}
	
	public double getCoordinate(String string, String searchString) {
		String coordinate;
		coordinate = string.substring(string.indexOf(searchString));
		coordinate = coordinate.substring(coordinate.indexOf(">") + 1);
		coordinate = coordinate.substring(0, coordinate.indexOf("<"));
		return Double.parseDouble(coordinate);
	}
	
	public String getTimeZoneIDFromXML(String timeZoneString) {
		String zoneID;
		zoneID = timeZoneString.substring(timeZoneString.indexOf("<time_zone_id>"));
		zoneID = zoneID.substring(zoneID.indexOf(">") + 1);
		zoneID = zoneID.substring(0, zoneID.indexOf("<"));
		return zoneID;
	}
	
	public boolean isSummerTime(String id) {
		Date date = new Date();
		return TimeZone.getTimeZone(id).inDaylightTime(date);
		
	}
	
	public String refactorLocation(String location) {
		String output = null;
        try {
            output = new String(location.getBytes("UTF-8"), "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        return output;
	}
}
