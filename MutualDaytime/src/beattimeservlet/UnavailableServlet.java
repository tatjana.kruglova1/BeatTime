package beattimeservlet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UnavailableServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String input = request.getParameter("input");
		String[] i = input.split("\t");
		String user = i[0];
		BufferedReader br = new BufferedReader(new FileReader("MutualDaytime/resources/unavailable.txt"));
		String line;
		String newContent = "";
		boolean notSet = true;
		while ((line = br.readLine()) != null) {
			if (line.split("\t")[0].equals(user)) {
				if (i.length > 1) {
					newContent += input + '\n';
				}
				notSet = false;
			} else {
				newContent += line + '\n';
			}
		}
		if(notSet) {
			newContent += input + '\n';
		}
		br.close();
		BufferedWriter bw = new BufferedWriter(new FileWriter("MutualDaytime/resources/unavailable.txt"));
		bw.write(newContent);
		bw.flush();
		bw.close();
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("Success");
		response.getWriter().flush();
		response.getWriter().close();
	}

}
