package beattimeservlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.FileInformation;
import daytime.TimesMatcher;

public class BeattimeServlet extends HttpServlet {
	TimesMatcher matcher = new TimesMatcher();
	FileInformation writer = new FileInformation();

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<String> locations = new ArrayList<String>();
		String result;
		if (request.getParameter("locationOne") != "" && request.getParameter("locationTwo") != null) {
			locations.add(request.getParameter("locationOne"));
			if (request.getParameter("locationTwo") != "" && request.getParameter("locationTwo") != null) {
				locations.add(request.getParameter("locationTwo"));
			}
			if (request.getParameter("locationThree") != "" && request.getParameter("locationThree") != null) {
				locations.add(request.getParameter("locationThree"));
			}
			if (request.getParameter("locationFour") != "" && request.getParameter("locationFour") != null) {
				locations.add(request.getParameter("locationFour"));
			}
			if (request.getParameter("locationFive") != "" && request.getParameter("locationFive") != null) {
				locations.add(request.getParameter("locationFive"));
			}

			ArrayList<String> times = matcher.getTimezones(locations);

			result = times.toString();
		} else {
			result = "ERROR";
		}
		
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(result);
		response.getWriter().flush();
		response.getWriter().close();
	}

}
