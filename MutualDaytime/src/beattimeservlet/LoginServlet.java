package beattimeservlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import database.FileInformation;

public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	FileInformation writer = new FileInformation();
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username;
		String password;
		String completeUserInfo = "";
		
		if (request.getParameter("username") != "" 
				&& request.getParameter("username") != null) {
			username = request.getParameter("username");
			password = request.getParameter("password");
			
			completeUserInfo = writer.readDataFromFile(username + '\t' + password);
		} else {
			
		}
		
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(completeUserInfo);
		response.getWriter().flush();
		response.getWriter().close();
	}

}
