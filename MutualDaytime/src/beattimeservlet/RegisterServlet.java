package beattimeservlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.FileInformation;

public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	FileInformation writer = new FileInformation();
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username;
		String password;
		String email;
		String completeUserInfo = "";
		
		if (request.getParameter("usernameRegister") != "" 
				&& request.getParameter("usernameRegister") != null) {
			username = request.getParameter("usernameRegister");
			password = request.getParameter("passwordRegister");
			email = request.getParameter("emailRegister");
			completeUserInfo = writer.checkUsernameAvailability(username);
			
			if (completeUserInfo.equals("SUCCESS")) {
				writer.createUser(username, password, email);
			}
			
		} else {
			
		}
		
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(completeUserInfo);
		response.getWriter().flush();
		response.getWriter().close();
	}
	
	/*public FileWriter getWriter() {
		return writer;
	}*/

}
