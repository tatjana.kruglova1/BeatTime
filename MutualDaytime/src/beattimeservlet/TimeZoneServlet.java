package beattimeservlet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TimeZoneServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String timeZone = request.getParameter("timeZone");
		String user = request.getParameter("user");
		BufferedReader br = new BufferedReader(new FileReader("MutualDaytime/resources/database.txt"));
		String line;
		String newContent = "";
		while ((line = br.readLine()) != null) {
			if (line.split("\t")[0].equals(user)) {
				String[] l = line.split("\t");
				newContent += l[0] + '\t' + l[1] + '\t' + l[2] + '\t' + timeZone + '\n';
			} else {
				newContent += line + '\n';
			}
		}
		br.close();
		BufferedWriter bw = new BufferedWriter(new FileWriter("MutualDaytime/resources/database.txt"));
		bw.write(newContent);
		bw.flush();
		bw.close();

		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("Success");
		response.getWriter().flush();
		response.getWriter().close();
	}

}