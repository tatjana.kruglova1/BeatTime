package daytime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Converter {
	
	private static final ArrayList<Double> AVAILABLEHOURS = new ArrayList<>(Arrays.asList(8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0));
	private ArrayList<Double> currentHours = new ArrayList<>();
	private boolean convertToZero = true;
	
	public Converter(ArrayList<String> comprableUTCs) {
		currentHours.addAll(AVAILABLEHOURS);
	}
	
	public double getUTCNumber(String timezone) {
		double minutes = Double.parseDouble(timezone.substring(4, 6))/60;
		double timezoneNumber = Double.parseDouble(timezone.substring(1,3));
		timezoneNumber += minutes;
		return timezoneNumber;
	}
	
	public String getUTCIndicator(String timezone) {
		String backOrForward = timezone.substring(0,1);
		return backOrForward;
	}
	
	public ArrayList<Double> convertHours(String indicator, double timezoneNumber) {
		ArrayList<Double> currentOriginalHours = new ArrayList<>();
		
		if ((indicator.equals("+") && !isToZero()) || (indicator.equals("-") && isToZero())) {
			for (int i = 0; i < currentHours.size(); i++) {
				if((currentHours.get(i) + timezoneNumber) >= 24.0) {
					currentOriginalHours.add((currentHours.get(i) + timezoneNumber) - 24.0);
				} else {
					currentOriginalHours.add(currentHours.get(i) + timezoneNumber);
				}
			}
			return currentOriginalHours;
		} else {
			for (int i = 0; i < currentHours.size(); i++) {
				if((currentHours.get(i) - timezoneNumber) < 0) {
					currentOriginalHours.add((currentHours.get(i) - timezoneNumber) + 24.0);
				} else {
					currentOriginalHours.add(currentHours.get(i) - timezoneNumber);
				}
			}
			return currentOriginalHours;
		}
		
	}

	public boolean isToZero() {
		return convertToZero;
	}

	public void setToZero(boolean toZero) {
		this.convertToZero = toZero;
	}
	
	public void setHours(ArrayList<Double> hours) {
		this.currentHours = hours;
	}
	
	public void removeUnavailableHours(String utc, HashMap<String, ArrayList<Double>> hours) {
		currentHours.clear();
		currentHours.addAll(AVAILABLEHOURS);
		ArrayList<Double> getHours = new ArrayList<>(hours.get(utc));
			for(Double unavailableHour: getHours) {
				currentHours.remove(unavailableHour - unavailableHour%1);
			}
	}
	
}
