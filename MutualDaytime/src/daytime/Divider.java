package daytime;

import java.util.ArrayList;

public class Divider {

	public ArrayList<ArrayList<Double>> divideHoursIntoParts(ArrayList<Double> times) {
		ArrayList<ArrayList<Double>> dividedHours = new ArrayList<>();
		ArrayList<Double> hours = new ArrayList<>();
		for (int i = 0; i < times.size()-1; i++) {
			if(times.get(i+1) - times.get(i) > 1.5 || times.get(i) - times.get(i+1) > 1.5) {
				hours.add(times.get(i));
				dividedHours.add(new ArrayList<Double>(hours));
				hours.clear();
			} else if (i+1 == times.size() - 1) {
				hours.add(times.get(i+1));
				if (hours.size() != 1) {
					dividedHours.add(new ArrayList<Double>(hours));
					hours.clear();
				}
			} else {
				hours.add(times.get(i));
			}
			
		}
		return dividedHours;
	}
	
	public ArrayList<Double> dividedUnavailableTimes(String times) {
		ArrayList<Double> unavailableHours = new ArrayList<Double>();
		String[] alltimes = times.split("\t");
		for(String t : alltimes) {
			String[] time = t.trim().split("-");
			String begginingTime = time[0];
			String[] foursAndMinutes = begginingTime.split(":");
			Double begHour = Double.parseDouble(foursAndMinutes[0]);
			begHour += Double.parseDouble(foursAndMinutes[1])/60;
			
			String endingTime = time[1];
			String[] foursAndMinutes2 = endingTime.split(":");
			Double endHour = Double.parseDouble(foursAndMinutes2[0]);
			endHour += Double.parseDouble(foursAndMinutes2[1])/60;
			
			while(begHour < endHour-1) {
				begHour += 1;
				unavailableHours.add(begHour);
			}
			
		}
		return unavailableHours;
	}
}
