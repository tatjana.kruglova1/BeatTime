package daytime;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import database.FileInformation;
import location.LocationSearch;

public class TimesMatcher {

	LocationSearch locationSearch = new LocationSearch();
	DaytimeFinder daytimeFinder = new DaytimeFinder();
	Converter converter;
	Divider divider = new Divider();
	FileInformation writer = new FileInformation();
	private HashMap<String,ArrayList<Double>> unavailableHours = new HashMap<>();
	private static final String FILENAME = "C:/Users/Tatjana/Downloads/myBeatTime/myBeatTime/MutualDaytime/resources/database.txt";

	public ArrayList<String> getTimezones(ArrayList<String> locations) {
		
		unavailableHours.clear();
		ArrayList<String> comparableUTCs = getTimezonesFromLocations(locations);

		converter = new Converter(comparableUTCs);

		ArrayList<ArrayList<Double>> groupOfTimez = createGroupOfTimes(comparableUTCs);

		ArrayList<Double> mutualZeroTime = daytimeFinder.compareHours(groupOfTimez);
		converter.setHours(mutualZeroTime);

		ArrayList<String> groupOfCorrectTimez = makeMutualTimePresentable(comparableUTCs);

		return groupOfCorrectTimez;
	}

	private ArrayList<String> makeMutualTimePresentable(ArrayList<String> comparableUTCs) {
		ArrayList<String> groupOfCorrectTimez = new ArrayList<>();

		for (String utc : comparableUTCs) {
			ArrayList<Double> times = new ArrayList<>();
			String indicator = converter.getUTCIndicator(utc);
			double number = converter.getUTCNumber(utc);
			converter.setToZero(false);
			times = converter.convertHours(indicator, number);
			ArrayList<ArrayList<Double>> timesDivided = divider.divideHoursIntoParts(times);

			String dividedMutualTimes = "";
			for (ArrayList<Double> eachDivision : timesDivided) {
				double num = eachDivision.get(0);
				int integerHour = (int) num;
				int decimalHour = (int) ((10 * num - 10 * integerHour) * 0.6);

				double num2 = eachDivision.get(eachDivision.size() - 1);
				int integerHour2 = (int) num2;
				int decimalHour2 = (int) ((10 * num2 - 10 * integerHour2) * 0.6);

				dividedMutualTimes += integerHour + ":" + decimalHour + "0-" + integerHour2 + ":" + decimalHour2 + "0\t";
			}
			if(!dividedMutualTimes.isEmpty()) {
				groupOfCorrectTimez.add(dividedMutualTimes);
			}

		}
		if (groupOfCorrectTimez.isEmpty()) {
			groupOfCorrectTimez.add("Could not find mutual time");
		}
		return groupOfCorrectTimez;
	}

	private ArrayList<ArrayList<Double>> createGroupOfTimes(ArrayList<String> comparableUTCs) {
		ArrayList<ArrayList<Double>> groupOfTimez = new ArrayList<>();

		for (String utc : comparableUTCs) {
			String indicator = converter.getUTCIndicator(utc);
			double number = converter.getUTCNumber(utc);
			converter.setToZero(true);
			if(unavailableHours.containsKey(utc)){
				converter.removeUnavailableHours(utc, unavailableHours);
			}
			groupOfTimez.add(converter.convertHours(indicator, number));
		}
		return groupOfTimez;
	}

	private ArrayList<String> getTimezonesFromLocations(ArrayList<String> locations) {
		ArrayList<String> comparableUTCs = new ArrayList<String>();

		for (String location : locations) {
			String utc = getTimeZoneFromFile(location);
			if (utc.equals("notUsername")) {
				utc = locationSearch.getUTCTimeZone(locationSearch.getTimeZoneID(location));
			}
			comparableUTCs.add(utc);

		}
		return comparableUTCs;
	}

	public String getTimeZoneFromFile(String username) {
		String timezone = "notUsername";
		String unavailableString;
		try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
			String line = br.readLine();

			while (line != null) {
				line = line.toString();
				if (line.startsWith(username)) {
					String[] elementsInLine = line.split("\t");
					timezone = elementsInLine[elementsInLine.length - 1];
					unavailableString = writer.checkUnavailableTimes(username);
					if(!unavailableString.equals("NO TIMES")) {
						unavailableHours.put(timezone, divider.dividedUnavailableTimes(unavailableString));
					}
					break;
				}
				line = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return timezone;
	}

}
