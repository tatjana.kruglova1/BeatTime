package daytime;

import java.util.ArrayList;

public class DaytimeFinder {

	ArrayList<ArrayList<Double>> group = new ArrayList<ArrayList<Double>>();

	public ArrayList<Double> compareHours(ArrayList<ArrayList<Double>> group) {
		ArrayList<Double> mutualTime = new ArrayList<Double>();

			
		for (Double hour : group.get(0)) {
			int groupsThatHaveIt = 1;
			Double mutualHour = null;
			int integerHour = (int)hour.doubleValue();
			double decimalHour = (10 * hour - 10 * integerHour)/10;
			for (int i = 1; i < group.size(); i++) {
				for (Double hour2 : group.get(i)) {
					int integerHour2 = (int)hour2.doubleValue();
					double decimalHour2 = (10 * hour2 - 10 * integerHour2)/10;
					if(integerHour == integerHour2) {
						groupsThatHaveIt++;
						if(decimalHour > decimalHour2) {
							if(mutualTime.isEmpty()) {
								mutualHour = (double) (integerHour + decimalHour2); 
							} else {
								mutualHour = (double) (integerHour + decimalHour); 
							}
							if(!mutualTime.contains(mutualHour) && groupsThatHaveIt == group.size()) {
								mutualTime.add(mutualHour);
								break;
							}
						} else {
							if(mutualTime.isEmpty()) {
								mutualHour = (double) (integerHour + decimalHour); 
							} else {
								mutualHour = (double) (integerHour + decimalHour2); 
							}
							if(!mutualTime.contains(mutualHour) && groupsThatHaveIt == group.size()) {
								mutualTime.add(mutualHour);
								break;
							}
						}
					}
				}
			}
		}
		//System.out.println(mutualTime);
		return mutualTime;
	}
	
}