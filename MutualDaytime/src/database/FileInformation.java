package database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import location.AutomaticLocation;

public class FileInformation {
	
	AutomaticLocation automaticLocation = new AutomaticLocation();
	private static final String FILENAME = "C:/Users/Tatjana/Downloads/myBeatTime/myBeatTime/MutualDaytime/resources/database.txt";
	private static final String UNAVAILABLE_FILE = "C:/Users/Tatjana/Downloads/myBeatTime/myBeatTime/MutualDaytime/resources/unavailable.txt";
	private OutputStream out;
	
	public OutputStream getOut() {
		return out;
	}

	public void createUser(String user, String password, String email) {
		try {
			out = new FileOutputStream(FILENAME, true);
			/*PrintStream output = new PrintStream(out);
			System.setOut(output);*/
			String timezone = automaticLocation.getWindowsTimezone();
			String content = user + "\t" + password + "\t" + email + "\t" + timezone + "\n";
			byte[] line = content.getBytes();
			
			out.write(line);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public String readDataFromFile(String matchString) {
		try(BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
		    String line = br.readLine();

		    while (line != null) {
		    	line = line.toString();
		    	String[] dataFromFile = line.split("\t");
		    	String[] dataFromString = matchString.split("\t"); 
		    	if (line.contains(matchString)) {
		    		return matchString;
		    	} else if (dataFromFile[0].equals(dataFromString[0])) {
		    		return "Wrong password";
		    	}
		        line = br.readLine();
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "false";
	}
	
	public String checkUsernameAvailability(String username) {
		try(BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
		    String line = br.readLine();

		    while (line != null) {
		    	line = line.toString();
		    	if (line.contains(username)) {
		    		return "ERROR";
		    	}
		        line = br.readLine();
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}
	
	public String checkUnavailableTimes(String username) {
		try(BufferedReader br = new BufferedReader(new FileReader(UNAVAILABLE_FILE))) {
		    String line = br.readLine();

		    while (line != null) {
		    	line = line.toString();
		    	if (line.startsWith(username)) {
		    		return line.substring(username.length()).trim();
		    	}
		        line = br.readLine();
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "NO TIMES";
	}
	
}
